import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { DiagnosisService } from '@app/shared/services/diagnosis.service';
import { Diagnosis } from '@app/shared/interfaces/diagnosis';
import { Triage } from './shared/interfaces/triage';
import { TriageService } from './shared/services/triage.service';

class Evidence {
  id: string;
  choice_id: string;
}
const DATA_NOT_VALID =
  'Información mal seleccionada, porfavor intentalo de nuevo cuidadosamente';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  resp: Diagnosis;
  triage: Triage;
  errorMsg: string;
  map = new Map<string, string>();
  isRequesting = false;
  gender: string = '';
  age: number = 0;
  isSignedUp = false;

  constructor(
    private diagnosisService: DiagnosisService,
    private triageService: TriageService
  ) {}

  ngOnInit() {
    console.log('age: ', this.age);
  }

  onSelectGender(gender: string) {
    this.gender = gender;
  }

  private setDiagnosis = (
    gender: string,
    age: number,
    evidence: Object[]
  ): void => {
    //console.log(evidence);
    this.isRequesting = true;

    //console.log(this.map);
    this.diagnosisService.postDiagnosis(gender, age, evidence).subscribe(
      (resp) => {
        this.isRequesting = false;
        this.errorMsg = null;
        // print
        console.log(resp);
        this.resp = resp;
        if (this.resp['question']) {
          this.resp.question.items.forEach((item) => {
            this.map.set(item.id, '');
          });
        }
      },
      (err) => {},
      () => {
        if (this.resp.should_stop) {
          this.setTriage(this.gender, this.age, evidence);
        }
      }
    );
  };

  private validateForm = (items : any)=>{
    let times:number=0;
    items.forEach((item)=>{
      if(this.map.get(item.id) ==''){
        times++;
      }
    })
    this.errorMsg= times>0 ? DATA_NOT_VALID : '';
    return times>0 ? false : true;
  };

  private setTriage = (
    gender: string,
    age: number,
    evidence: Object[]
  ): void => {
    this.triageService.postTriage(gender, age, evidence).subscribe((triage) => {
      console.log('Respuesta: ', triage);
      this.triage = triage;
    });
  };

  onSubmit = (form: NgForm) => {
    console.log('gender ', this.gender, ' age: ', this.age);
    if (this.gender != '' && this.age >= 0) {
      if (this.isSignedUp == false) {
        this.setDiagnosis(this.gender, this.age, []);
        this.isSignedUp = true;
      } else {
        let evidence: Object[] = [];
        this.map.forEach((v, k) => {
          if (this.resp.question.type == 'group_single') {
            let id = form.value['group_single'];
            if (id == '') {
              this.errorMsg = DATA_NOT_VALID;
            } else {
              v = k == id ? 'present' : 'absent';
            }
          } else {
            let value = form.value[k];
            if (value) {
              v = value;
            }
          }
          this.map.set(k, v);
          evidence.push({ id: k, choice_id: v });
        });
        if(this.validateForm(this.resp.question.items))  // validate all input are filled
          this.setDiagnosis(this.gender, this.age, evidence);
      }
    } else {
      this.errorMsg = DATA_NOT_VALID;
    }
  };
}
