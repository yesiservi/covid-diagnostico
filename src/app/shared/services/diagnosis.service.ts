import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { Diagnosis } from '@app/shared/interfaces/diagnosis';

@Injectable({
  providedIn: 'root'
})
export class DiagnosisService {
  private diagnosisURL = `${environment.api}/diagnosis`;

  constructor(private http: HttpClient) {}

  postDiagnosis = (sex: string, age: number, evidence: Object[]): Observable<Diagnosis> => {
    const body = {
      sex,
      age,
      evidence,
    }
    const headers = {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json'
    };

    return this.http.post<Diagnosis>(`${this.diagnosisURL}`, body, { headers });
  };

  // getCaseDocuments = (
  //   caseId: string,
  //   params: HttpParams = new HttpParams()
  // ): Observable<Document[]> => {
  //   const headers = {
  //     'Content-Type': 'application/json; charset=utf-8',
  //     Accept: 'application/json'
  //   };
  //   return this.http.get<Document[]>(`${this.documentURL}/${caseId}`, { params, headers });
  // };

  // getTypes = (params: HttpParams = new HttpParams()): Observable<DocumentType[]> => {
  //   const headers = {
  //     'Content-Type': 'application/json; charset=utf-8',
  //     Accept: 'application/json'
  //   };
  //   return this.http.get<DocumentType[]>(`${this.documentURL}/types`, { params, headers });
  // };
}
