import { environment } from '@environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Triage } from '@app/shared/interfaces/triage';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class TriageService {
    private diagnosisURL = `${environment.api}/triage`;

    constructor(private http: HttpClient) {}

    postTriage = (sex: string, age: number, evidence: Object[]): Observable<Triage> => {
        const body = {
            sex,
            age,
            evidence,
          }
          const headers = {
            'Content-Type': 'application/json; charset=utf-8',
            Accept: 'application/json'
          };      
        
        return this.http.post<Triage>(`${this.diagnosisURL}`, body);
    }


}