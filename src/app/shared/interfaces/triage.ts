class Serious {
    common_name: string;
    id: string;
    is_emergency: boolean;
    name: string;
}

export class Triage {
    description: string;
    label: string;
    serious: Serious[];
    triage_level: string;
}